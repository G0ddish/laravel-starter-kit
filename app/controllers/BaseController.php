<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
        define("APPNAME", "BarCrawl v1");
        if ( ! is_null($this->layout))
		{

			$this->layout = View::make($this->layout);
		}
	}

}
